<?php
/**
 * Calculate Forms
 * User: omeripek.com.tr
 * Date: 23.04.2017
 * Time: 11:13
 */

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	$connect_web = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');
	
	$usd_satis = trim(htmlspecialchars($connect_web->Currency[0]->BanknoteSelling));
	

    if(empty($_POST['genislik']) || empty($_POST['yukseklik']) || !ctype_digit(strval($_POST['genislik'])) || !ctype_digit(strval($_POST['yukseklik'])) ){
        echo '<b>Lütfen Ölçü Giriniz! <br /> Girdiğiniz değer "cm" olarak hesaplanacaktır!</b>';
        die();
    } else {

        $malzeme = trim(htmlspecialchars($_POST['malzeme']));
        $genislik = trim(htmlspecialchars($_POST['genislik']));
        $yukseklik = trim(htmlspecialchars($_POST['yukseklik']));
        $dikimlimi = trim(htmlspecialchars($_POST['dikimlimi']));

        if($malzeme == "3"){
            $malzemeFiyat = 3.5;
        } elseif ($malzeme == "6") {
             $malzemeFiyat = 5;
        } else {
            $malzemeFiyat = 4;
        }

        function digitalCalculator($malzemeFiyat,$genislik,$yukseklik,$dikimlimi,$usd_satis) {
             $olcu = (($genislik / 100) * ($yukseklik / 100));
             $fireliOlcu = $olcu + ($olcu * 0.10);
             $tahminiFiyat = (($fireliOlcu * $malzemeFiyat * $usd_satis) + (2 * $fireliOlcu * $dikimlimi));
             return floor($tahminiFiyat).' TL';
        }


        echo '<strong>Tahmini Fiyat : </strong> '.digitalCalculator($malzemeFiyat,$genislik,$yukseklik,$dikimlimi,$usd_satis);
        die();

    }

}
else {
    header('location:dijital-baski-fiyati-hesapla');
    die();
}